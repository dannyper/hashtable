package hashtable

import (
	"hash/fnv"
)

const defaultSize = 3
const minLoadFactor = 0.25
const maxLoadFactor = 0.75

type Element struct {
	hash int
	key  string
	val  string
	next *Element
}

type Table struct {
	elements []*Element
}

type HashTable struct {
	table     Table
	nElements int
}

func (t *HashTable) Append(k string, v string) {
	if t.table.elements == nil {
		*t = createTable(defaultSize)
	}
	t.append(hash(k), k, v)
	t.updateTableSize()

	return
}

func (t *HashTable) Find(k string) string {
	index := hash(k) % len(t.table.elements)
	element := t.table.elements[index]

	for element != nil {
		if element.key == k {
			return element.val
		}
		element = element.next
	}
	return ""
}

func (t *HashTable) Delete(k string) bool {
	index := hash(k) % len(t.table.elements)
	element := t.table.elements[index]
	var prev *Element

	for element != nil {
		if element.key == k {
			if prev != nil {
				prev.next = element.next
			} else {
				t.table.elements[index] = element.next
			}
			t.nElements--
			t.updateTableSize()
			return true

		}
		prev = element
		element = element.next
	}

	return false
}

func hash(s string) int {
	h := fnv.New64()
	_, _ = h.Write([]byte(s))
	return int(h.Sum64() >> 1)
}

func (t *HashTable) updateTableSize() {
	if t.nElements == 0 {
		return
	}
	var newTable HashTable

	newSize, changed := t.calculateSize()
	if !changed {
		return
	}
	newTable = createTable(newSize)

	for _, element := range t.table.elements {
		for element != nil {
			newTable.append(element.hash, element.key, element.val)
			element = element.next
		}
	}
	*t = newTable
	return
}

func (t *HashTable) append(h int, k string, v string) {
	index := h % len(t.table.elements)
	element := t.table.elements[index]
	newElement := Element{h, k, v, nil}

	if element == nil {
		t.table.elements[index] = &newElement
		t.nElements++
		return
	}

	prev := &Element{0, "", "", nil}
	for element != nil {
		if element.key == k {
			element.val = v
			return
		}
		prev = element
		element = element.next
	}
	prev.next = &newElement
	t.nElements++

	return
}

func createTable(size int) HashTable {
	return HashTable{table: Table{elements: make([]*Element, size)}, nElements: 0}
}

func (t *HashTable) calculateSize() (size int, changed bool) {
	loadFactor := float64(t.nElements) / float64(len(t.table.elements))

	if loadFactor < minLoadFactor {
		size = len(t.table.elements) / 2

		if size <= defaultSize {
			size = defaultSize
		}
		changed = true
		return
	}

	if loadFactor > maxLoadFactor {
		size = t.nElements * 2
		changed = true
		return
	}
	return
}
