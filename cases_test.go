package hashtable

type element struct {
	key   string
	value string
}

var appendTestCases = []struct {
	description string
	input       []element
	expected    HashTable
}{
	{
		description: "empty",
		input: []element{
			{"", ""},
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, nil, {7347990519673328018, "", "", nil}}},
			nElements: 1,
		},
	},
	{
		description: "1 element",
		input: []element{
			{"BLU", "Blue"},
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, {7797495723580726450, "BLU", "Blue", nil}, nil}},
			nElements: 1,
		},
	},
	{
		description: "2 elements with 1 coalition",
		input: []element{
			{"BLU", "Blue"},
			{"RED", "Red"},
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, {7797495723580726450, "BLU", "Blue", &Element{7789908543591770185, "RED", "Red", nil}}, nil}},
			nElements: 2,
		},
	},
	{
		description: "3 elements with duplicated key",
		input: []element{
			{"BLU", "Navy Blue"},
			{"BLU", "Blue"},
			{"RED", "Red"},
			{"PUR", "Purple"},
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, {7789908543591770185, "RED", "Red", nil}, nil, nil, {7797495723580726450, "BLU", "Blue", nil}, {7790785953870893333, "PUR", "Purple", nil}}},
			nElements: 3,
		},
	},
	{
		description: "3 elements with 3 coalition",
		input: []element{
			{"BLU", "Navy Blue"},
			{"BLA", "Black"},
			{"WHI", "White"},
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, nil, nil, nil, {7797495723580726450, "BLU", "Navy Blue", &Element{7797495723580726456, "BLA", "Black", &Element{7787515456533442012, "WHI", "White", nil}}}, nil}},
			nElements: 3,
		},
	},
	{
		description: "3 elements with 3 coalition and duplicated key",
		input: []element{
			{"BLU", "Navy Blue"},
			{"BLA", "Black"},
			{"WHI", "White"},
			{"BLA", "Dark Black"},
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, nil, nil, nil, {7797495723580726450, "BLU", "Navy Blue", &Element{7797495723580726456, "BLA", "Dark Black", &Element{7787515456533442012, "WHI", "White", nil}}}, nil}},
			nElements: 3,
		},
	},
	{
		description: "4 elements with 1 coalition and 1x slice growing",
		input: []element{
			{"BLU", "Navy Blue"},
			{"RED", "Red"},
			{"PUR", "Purple"},
			{"GRE", "Green"},
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, {7789908543591770185, "RED", "Red", nil}, nil, nil, {7797495723580726450, "BLU", "Navy Blue", nil}, {7790785953870893333, "PUR", "Purple", &Element{7795111982371238069, "GRE", "Green", nil}}}},
			nElements: 4,
		},
	},
	{
		description: "9 elements with 2 coalition and 3x slice growing",
		input: []element{
			{"BLU", "Navy Blue"},
			{"RED", "Red"},
			{"PUR", "Purple"},
			{"GRE", "Green"},
			{"YEL", "Yellow"},
			{"WHI", "White"},
			{"BLA", "Black"},
			{"AMB", "Amber"},
			{"PIN", "Pink"},
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, nil, {7797495723580726450, "BLU", "Navy Blue", nil}, nil, nil, {7790785953870893333, "PUR", "Purple", &Element{7795111982371238069, "GRE", "Green", nil}}, nil, nil, {7797495723580726456, "BLA", "Black", nil}, {7789908543591770185, "RED", "Red", &Element{7790792550940662601, "PIN", "Pink", nil}}, nil, {7797970162848204827, "AMB", "Amber", nil}, {7786566028242671100, "YEL", "Yellow", &Element{7787515456533442012, "WHI", "White", nil}}, nil, nil, nil}},
			nElements: 9,
		},
	},
}

var deleteTestCases = []struct {
	description string
	input       []string
	original    HashTable
	expected    HashTable
}{
	{
		description: "1 element",
		input:       []string{"BLU"},
		original: HashTable{
			table:     Table{[]*Element{nil, {7797495723580726450, "BLU", "Blue", nil}, nil}},
			nElements: 1,
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, nil, nil}},
			nElements: 0,
		},
	},
	{
		description: "Remove from head of linked list",
		input:       []string{"RED"},
		original: HashTable{
			table:     Table{[]*Element{nil, {7789908543591770185, "RED", "Red", &Element{7797495723580726450, "BLU", "Blue", nil}}, nil}},
			nElements: 2,
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, {7797495723580726450, "BLU", "Blue", nil}, nil}},
			nElements: 1,
		},
	},
	{
		description: "Remove from tail of linked list",
		input:       []string{"BLU"},
		original: HashTable{
			table:     Table{[]*Element{nil, {7789908543591770185, "RED", "Red", &Element{7797495723580726450, "BLU", "Blue", nil}}, nil}},
			nElements: 2,
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, {7789908543591770185, "RED", "Red", nil}, nil}},
			nElements: 1,
		},
	},
	{
		description: "shrink table",
		input:       []string{"GRE", "BLU"},
		original: HashTable{
			table:     Table{[]*Element{nil, {7790792550940662601, "PIN", "Pink", nil}, nil, nil, {7797495723580726450, "BLU", "Blue", nil}, {7795111982371238069, "GRE", "Green", nil}}},
			nElements: 3,
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, {7790792550940662601, "PIN", "Pink", nil}, nil}},
			nElements: 1,
		},
	},
}

var findTestCases = []struct {
	description string
	input       string
	original    HashTable
	expected    string
}{
	{
		description: "search in linked list",
		input:       "RED",
		original: HashTable{
			table:     Table{[]*Element{nil, {7789908543591770185, "RED", "Red", &Element{7797495723580726450, "BLU", "Blue", nil}}, nil}},
			nElements: 2,
		},
		expected: "Red",
	},
	{
		description: "2 elements table",
		input:       "PUR",
		original: HashTable{
			table:     Table{[]*Element{nil, {7789908543591770185, "RED", "Red", nil}, {7790785953870893333, "PUR", "Purple", nil}}},
			nElements: 4,
		},
		expected: "Purple",
	},
	{
		description: "dont exist",
		input:       "YEL",
		original: HashTable{
			table:     Table{[]*Element{nil, {7789908543591770185, "RED", "Red", nil}, {7790785953870893333, "PUR", "Purple", nil}}},
			nElements: 4,
		},
		expected: "",
	},
}
